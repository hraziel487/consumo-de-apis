<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Demo consumo de APIs</title>
  <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">  </head>
  <body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg ">
  <div class="container-fluid">
  <a class="navbar-brand" href="#">
      <img href="https://racielhernandez.com/" src="https://racielhernandez.com/wp-content/uploads/2022/05/7198455802_be26de6c-95d0-452a-a3e5-3bf20a7cee9c-1.png" alt="Logo" width="30" height="30" class="d-inline-block align-text-top">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="https://racielhernandez.com/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/">Poke Api</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/rickymorty">Rick y Morty</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/albumes">Theaudiodb</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
@yield('content')

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>  </body>
</html>