@extends('layouts.app')
@section('content')
<header>
  <!-- Jumbotron -->
  <div class="p-5 text-center bg-dark">
    <h1 class="mb-3 text-white">Consumo de API pokemones</h1>
  </div>
  <!-- Jumbotron -->
</header>




    <div class="row row-cols-1 row-cols-sm-3 g-4 w-75 mx-auto mt-5">
    @foreach ($pokemon as $poke)
    <div class="col">
    <div class="card h-100">
      <img src="{{ $poke['imagen1']}}" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title text-center">{{ $poke['nombre']}}</h5>
        <div class="container">
        <button type="button" class="btn btn-primary position-relative">
              Habilidad: {{ $poke['habilidad']}} <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-secondary"></span>
        </button>

        <button type="button" class="btn btn-dark position-relative">
        Tipo: {{ $poke['tipo']}} <svg width="1em" height="1em" viewBox="0 0 16 16" class="position-absolute top-100 start-50 translate-middle mt-1" fill="#212529" xmlns="http://www.w3.org/2000/svg"><path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/></svg>
        </button>

        <button type="button" class="btn btn-danger  container mt-2 position-relative">
         Movimiento: {{ $poke['movimiento']}} <span class="position-absolute top-0 start-100 translate-middle badge border border-light rounded-circle bg-danger p-2"></span>
        </button>
</div>
      </div>
    </div>
  </div>
 @endforeach
 </div>
 <footer class="bg-dark text-center text-lg-start mt-5">
  <!-- Copyright -->
  <div class="text-center p-3 text-white" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2022 Copyright:
    <a class="text-white" href="https://racielhernandez.com/">racielhernandez.com</a>
  </div>
  <!-- Copyright -->
</footer>
@endsection