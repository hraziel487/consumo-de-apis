<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    public function pokemones()
    {
        $response = Http::get('https://pokeapi.co/api/v2/pokemon?offset=0&limit=21');
        $datos = $response->json();

        $pokemon = [];

        foreach ($datos['results'] as $poke) {
            
            $url = $poke['url'];
            $response = Http::get($url);
            $data = $response->json();
            $pokemon[] = [
                'nombre'=>$poke['name'],
                'imagen1'=> $data['sprites']['other']['home']['front_default'],
                'habilidad'=> $data['abilities']['0']['ability']['name'],
                'movimiento'=> $data['moves']['0']['move']['name'],
                'tipo'=> $data['types']['0']['type']['name'],
            ];
        }
        
        return view('home', ['pokemon'=>$pokemon]);
        }

        public function RickyMorty()
    {
        $response = Http::get('https://rickandmortyapi.com/api/character');
        $datos = $response->json();

        $personajes = [];

        foreach ($datos['results'] as $personaje) {
            
            $url = $personaje['url'];
            $response = Http::get($url);
            $data = $response->json();
            $personajes[] = [
                'nombre'=>$personaje['name'],
                'imagen'=> $data['image'],
                'estatus'=> $data['status'],
                'especie'=> $data['species'],
                'genero'=> $data['gender'],
            ];
        }
        
        return view('rickymorty', ['personajes'=>$personajes]);
        }


        public function  albumes()
    {
        $response = Http::get('https://theaudiodb.com/api/v1/json/2/album.php?i=136149');
        $datos = $response->json();

        $albumes = [];

        foreach ($datos['album'] as $album) {
            $albumes[] = [
                'nombre'=>$album['strAlbumStripped'],
                'imagen'=> $album['strAlbumThumb'],
                'album'=> $album['strArtist'],
                'lanzamiento'=> $album['intYearReleased'],
                'genero'=> $album['strGenre'],
            ];
        }
        
        return view('albumes', ['albumes'=>$albumes]);

    }
}
